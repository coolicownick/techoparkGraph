//
// Created by nickolas on 23.12.2021.
//

#include "IGraph.h"
#include "queue"

void BFS(const IGraph& graph,int vertex,void (*visit)(int)){
    std::vector<bool> visited(graph.VerticesCount(), false);
    std::queue<int> bfsQueue;
    bfsQueue.push(vertex);
    visited[vertex] = true;
    while (bfsQueue.size() > 0){
        int current = bfsQueue.front();
        bfsQueue.pop();
        visit(current);
        std::vector<int> list = graph.GetNextVertices(current);
        for( int i = 0; i < list.size(); ++i){
            if(!visited[list[i]]){
                bfsQueue.push(list[i]);
                visited[list[i]] = true;
            }
        }
    }
}
