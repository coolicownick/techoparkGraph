//
// Created by nickolas on 23.12.2021.
//

#include "SetGraph.h"
#include "assert.h"

SetGraph::SetGraph(int vertexCount):
    set(vertexCount)
    {}

SetGraph::SetGraph(const IGraph &graph):
    set(graph.VerticesCount(),std::unordered_set<int,std::hash<int>>()){
    for(int i = 0; i < graph.VerticesCount(); ++i){
        for(auto vertex: graph.GetNextVertices(i))
            set[i].insert(vertex);
    }
}

void SetGraph::AddEdge(int from, int to) {
    assert( from >= 0 && from < set.size());
    assert( to >= 0 && to < set.size());
    set[from].insert(to);
    set[to].insert(from);
}

int SetGraph::VerticesCount() const {
    return set.size();
}

std::vector<int> SetGraph::GetNextVertices(int vertex) const {
    assert( vertex >= 0 && vertex < set.size());
    std::vector<int> vertices(0);
    for(auto next_vertex : set[vertex])
        vertices.insert(vertices.begin(),next_vertex);
    return vertices;
}

std::vector<int> SetGraph::GetPrevVertices(int vertex) const {
    assert( vertex >= 0 && vertex < set.size());
    std::vector<int> vertices(0);
    for(int i = 0; i < set.size(); ++i){
        if(set[i].find(vertex) != set[i].end())
            vertices.insert(vertices.begin(),i);
    }
    return vertices;
}