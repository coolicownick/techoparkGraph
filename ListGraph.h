//
// Created by nickolas on 23.12.2021.
//

#ifndef GRAPH1_LISTGRAPH_H
#define GRAPH1_LISTGRAPH_H

#include "IGraph.h"

class ListGraph : public IGraph{
public:
    explicit ListGraph(int vertexCount);
    explicit ListGraph(const IGraph &graph);

    virtual void AddEdge(int from, int to) override;
    virtual int VerticesCount() const override;
    virtual std::vector<int> GetNextVertices(int vertex) const override;
    virtual std::vector<int> GetPrevVertices(int vertex) const override;

private:
    std::vector<std::vector<int>> lists;

};




#endif //GRAPH1_LISTGRAPH_H
