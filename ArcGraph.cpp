//
// Created by nickolas on 23.12.2021.
//

#include "ArcGraph.h"
#include "assert.h"
ArcGraph::ArcGraph(int vertexCount):
    vCount(vertexCount) {
}
ArcGraph::ArcGraph(const IGraph &graph):
    vCount(graph.VerticesCount()){
    for(int i = 0; i < vCount; ++i){
        for( auto next : graph.GetNextVertices(i)){
            arc.push_back({i,next});
        }
    }
}

void ArcGraph::AddEdge(int from, int to) {
    assert( from >= 0 && from < vCount);
    assert( to >= 0 && to < vCount);
    arc.push_back({from,to});
}

int ArcGraph::VerticesCount() const {   return vCount; }

std::vector<int> ArcGraph::GetNextVertices(int vertex) const {
    std::vector<int> vertices(0);
    for(auto pair:arc){
        if(pair.first == vertex)
            vertices.push_back(pair.second);
    }
    return vertices;
}


std::vector<int> ArcGraph::GetPrevVertices(int vertex) const {
    std::vector<int> vertices(0);
    for(auto pair:arc){
        if(pair.second == vertex)
            vertices.push_back(pair.first);
    }
    return vertices;
}