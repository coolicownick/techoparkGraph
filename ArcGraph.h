//
// Created by nickolas on 23.12.2021.
//

#ifndef GRAPH1_ARCGRAPH_H
#define GRAPH1_ARCGRAPH_H

#include "IGraph.h"

class ArcGraph : public IGraph{
public:
    explicit ArcGraph(int vertexCount);
    explicit ArcGraph(const IGraph &graph);

    virtual void AddEdge(int from, int to) override;
    virtual int VerticesCount() const override;
    virtual std::vector<int> GetNextVertices(int vertex) const override;
    virtual std::vector<int> GetPrevVertices(int vertex) const override;

private:
    int vCount;
    std::vector<std::pair<int,int>> arc;

};

#endif //GRAPH1_ARCGRAPH_H
