//
// Created by nickolas on 23.12.2021.
//

#ifndef GRAPH1_SETGRAPH_H
#define GRAPH1_SETGRAPH_H


#include "IGraph.h"
#include "unordered_set"

class SetGraph : public IGraph{
public:
    explicit SetGraph(int vertexCount);
    explicit SetGraph(const IGraph &graph);

    virtual void AddEdge(int from, int to) override;
    virtual int VerticesCount() const override;
    virtual std::vector<int> GetNextVertices(int vertex) const override;
    virtual std::vector<int> GetPrevVertices(int vertex) const override;

private:
   std::vector<std::unordered_set<int,std::hash<int>>> set;

};


#endif //GRAPH1_SETGRAPH_H
