//
// Created by nickolas on 23.12.2021.
//

#ifndef GRAPH1_MATRIXGRAPH_H
#define GRAPH1_MATRIXGRAPH_H


#include "IGraph.h"

class MatrixGraph : public IGraph{
public:
    explicit MatrixGraph(int vertexCount);
    explicit MatrixGraph(const IGraph &graph);

    virtual void AddEdge(int from, int to) override;
    virtual int VerticesCount() const override;
    virtual std::vector<int> GetNextVertices(int vertex) const override;
    virtual std::vector<int> GetPrevVertices(int vertex) const override;

private:
    std::vector<std::vector<bool>> matrix;

};


#endif //GRAPH1_MATRIXGRAPH_H
