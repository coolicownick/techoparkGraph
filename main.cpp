#include <iostream>
#include "ListGraph.h"
#include "MatrixGraph.h"
#include "SetGraph.h"
#include "ArcGraph.h"
#include "IGraph.h"


void run(){
    ListGraph listGraph(5);
    listGraph.AddEdge(0,1);
    listGraph.AddEdge(0,2);
    listGraph.AddEdge(0,3);
    listGraph.AddEdge(1,3);
    listGraph.AddEdge(2,3);
    listGraph.AddEdge(2,4);

    BFS(listGraph,0,[](int vertex){std::cout << vertex << " ";});
    std::cout << std::endl;
    ArcGraph arcGraph(listGraph);
    arcGraph.AddEdge(1,2);
    MatrixGraph matrixGraph(arcGraph);
    matrixGraph.AddEdge(1,4);
    SetGraph setGraph(matrixGraph);
    setGraph.AddEdge(3,4);
    ListGraph newListGraph(setGraph);

    BFS(arcGraph,0,[](int vertex){std::cout << vertex << " ";});
    std::cout << std::endl;
    BFS(matrixGraph,0,[](int vertex){std::cout << vertex << " ";});
    std::cout << std::endl;
    BFS(setGraph,0,[](int vertex){std::cout << vertex << " ";});
    std::cout << std::endl;
    BFS(newListGraph,0,[](int vertex){std::cout << vertex << " ";});

}

int main() {
    run();
    return 0;
}
