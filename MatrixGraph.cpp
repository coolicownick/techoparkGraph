//
// Created by nickolas on 23.12.2021.
//

#include "MatrixGraph.h"
#include "assert.h"

MatrixGraph::MatrixGraph(int vertexCount):
    matrix(vertexCount,std::vector<bool>(vertexCount, false))
    {}

MatrixGraph::MatrixGraph(const IGraph &graph):
    matrix(graph.VerticesCount(),std::vector<bool>(graph.VerticesCount(), false)){
    for(int i = 0; i < graph.VerticesCount(); ++i){
        for(auto vertex : graph.GetNextVertices(i))
            matrix[i][vertex] = true;
    }
}

void MatrixGraph::AddEdge(int from, int to) {
    assert( from >= 0 && from < matrix.size());
    assert( to >= 0 && to < matrix.size());
    matrix[from][to] = true;
}

int MatrixGraph::VerticesCount() const {
    return matrix.size();
}

std::vector<int> MatrixGraph::GetNextVertices(int vertex) const {
    assert(vertex >= 0 && vertex < matrix.size());
    std::vector<int> vertices;
    for (int i = 0; i < matrix.size(); ++i) {
        if (matrix[vertex][i])
            vertices.push_back(i);
    }
    return vertices;
}

std::vector<int> MatrixGraph::GetPrevVertices(int vertex) const {
    assert(vertex >= 0 && vertex < matrix.size());
    std::vector<int> vertices;
    for (int i = 0; i < matrix.size(); ++i) {
        if (matrix[i][vertex])
            vertices.push_back(i);
    }
    return vertices;
}